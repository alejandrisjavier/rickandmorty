package com.example.rickandmortyapi.interfaces

import com.example.rickandmortyapi.model.Character
import com.example.rickandmortyapi.model.CharacterList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

interface InterfaceApi {

    @GET("api/character")
    suspend fun getCharacters(@Query("page") page: Int): Response<CharacterList>

    @GET("api/character/{id}")
    suspend fun getCharacterId(@Path("id") id: Int): Response<Character>//deberia ser un solo modelo no una lista
}