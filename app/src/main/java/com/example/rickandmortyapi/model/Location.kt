package com.example.rickandmortyapi.model

data class Location(
    var name: String,
    var url: String
)
