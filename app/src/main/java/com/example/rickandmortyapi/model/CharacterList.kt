package com.example.rickandmortyapi.model

data class CharacterList(
    var results: List<Character>
)
