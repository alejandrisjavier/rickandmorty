package com.example.rickandmortyapi.repository

import com.example.rickandmortyapi.model.Character
import com.example.rickandmortyapi.model.CharacterList
import com.example.rickandmortyapi.services.RetrofitInstance
import retrofit2.Response

class Repository {

    suspend fun getCharacters(page: Int): Response<CharacterList> {
        return RetrofitInstance.api.getCharacters(page)
    }

    suspend fun getCharacterId(id: Int): Response<Character> {
        return RetrofitInstance.api.getCharacterId(id)
    }
}