package com.example.rickandmortyapi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.rickandmortyapi.databinding.ItemCharacterBinding
import com.example.rickandmortyapi.model.Character

class CharacterAdapter(private val lischaracters:List<Character>,private val itemClickListener: OnItemClickListener): RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return CharacterViewHolder(layoutInflater.inflate(R.layout.item_character,parent,false))
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.bind(lischaracters[position], itemClickListener)
    }

    override fun getItemCount(): Int {
        return lischaracters.size
    }

    class CharacterViewHolder(private val view: View) : RecyclerView.ViewHolder(view)
    {
        private val binding = ItemCharacterBinding.bind(view)

        fun bind(character: Character, clickListener: OnItemClickListener){

            binding.name.text = character.name
            binding.statusspecies.text = view.context.getString(R.string.character_status_species,character.status,character.species)
            binding.location.text = character.location.name
            Glide.with(view).load(character.image).into(binding.imagecharacter)
            view.setOnClickListener { clickListener.onItemClicked(character.id) }

        }
    }

    interface OnItemClickListener {
        fun onItemClicked(id : Int)
    }
}

