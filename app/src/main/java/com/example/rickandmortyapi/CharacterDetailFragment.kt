package com.example.rickandmortyapi

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.rickandmortyapi.databinding.FragmentCharacterDetailBinding
import com.example.rickandmortyapi.model.Character
import com.example.rickandmortyapi.repository.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class CharacterDetailFragment : Fragment() {

    private lateinit var binding : FragmentCharacterDetailBinding
    private val reposotorio : Repository = Repository()
    private var id : Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id= it.getInt(CharacterListFragment.ARGUMENT_ID_CHARACTER,0)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCharacterDetailBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        id?.let {
            getCharterId(id ?: 0)
        }
    }

    private fun getCharterId(id:Int)
    {
        binding.progressBar.visibility = View.VISIBLE
        CoroutineScope(Dispatchers.IO).launch {

            val call : Response<Character> = reposotorio.getCharacterId(id)
            val character: Character? = call.body()
            activity?.let {
                requireActivity().runOnUiThread {
                    when {
                        call.isSuccessful -> {

                            character?.let {
                                binding.textviewName.text = character.name
                                Glide.with(requireContext()).load(character.image)
                                    .into(binding.imgdetail)

                                binding.statusspecies.text = getString(R.string.character_status_species,character.status,character.species)
                                binding.gender.text = character.gender
                                binding.origen.text =  character.origin.name
                                binding.location.text = character.location.name

                                Log.d("UNPERSONAJE", character.toString())
                                binding.progressBar.visibility = View.GONE

                            }

                        }
                        else -> {
                            showError()
                        }
                    }
                }
            }
        }
    }

    private fun showError()
    {
        Toast.makeText(requireContext(),"Algo falló al obtener el Personaje", Toast.LENGTH_LONG).show()
    }

}