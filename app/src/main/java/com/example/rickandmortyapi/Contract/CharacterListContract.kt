package com.example.rickandmortyapi.Contract

import com.example.rickandmortyapi.model.Character

interface CharacterListContract {

    interface View{}

    interface Presenter{
        fun star()
        fun getCharactersPage(page: Int, callback: (List<Character>?)->Unit)
    }
}