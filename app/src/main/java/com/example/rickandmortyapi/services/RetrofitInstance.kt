package com.example.rickandmortyapi.services

import com.example.rickandmortyapi.interfaces.InterfaceApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl("https://rickandmortyapi.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val api: InterfaceApi by lazy {
        retrofit.create(InterfaceApi::class.java)
    }
}