package com.example.rickandmortyapi

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.rickandmortyapi.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding
    private lateinit var fragmentcharacterlist : CharacterListFragment
    private lateinit var fragmentcharacterdetail: CharacterDetailFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if(findViewById<View>(R.id.fragment_container)!=null) {
            fragmentcharacterlist = CharacterListFragment()
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container,fragmentcharacterlist).commit()
        }
        else
        {
            fragmentcharacterlist = CharacterListFragment()
            supportFragmentManager.beginTransaction().replace(R.id.fragment_list,fragmentcharacterlist).commit()

            fragmentcharacterdetail = CharacterDetailFragment()
            supportFragmentManager.beginTransaction().replace(R.id.fragment_detail,fragmentcharacterdetail).commit()
        }

    }

    fun showdetail(fragment : Fragment, bundle: Bundle){
        fragment.arguments = bundle

        if(findViewById<View>(R.id.fragment_container)!=null)
        {
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).addToBackStack(null).commit()
        }
        else
        {
            supportFragmentManager.beginTransaction().replace(R.id.fragment_detail,fragment).addToBackStack(null).commit()
        }
    }
}