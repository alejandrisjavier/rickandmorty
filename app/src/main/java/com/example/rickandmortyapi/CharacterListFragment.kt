package com.example.rickandmortyapi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rickandmortyapi.Contract.CharacterListContract
import com.example.rickandmortyapi.databinding.FragmentCharacterListBinding
import com.example.rickandmortyapi.model.Character
import com.example.rickandmortyapi.presenter.CharacterListPresenter
import com.example.rickandmortyapi.repository.Repository

class CharacterListFragment : Fragment(), CharacterListContract.View {

    private lateinit var binding: FragmentCharacterListBinding
    private lateinit var presenter : CharacterListContract.Presenter

    private lateinit var adapter: CharacterAdapter
    private var characterslist = mutableListOf<Character>()
    private var page: Int = FIRTS_PAGE;

    private val adapterItemClickListener = object : CharacterAdapter.OnItemClickListener {
        override fun onItemClicked(id: Int) {
            var bundle: Bundle = bundleOf(
                ARGUMENT_ID_CHARACTER to id
            )
            (activity as? MainActivity?)?.showdetail(CharacterDetailFragment(),bundle)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =FragmentCharacterListBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //init presenter
        presenter = CharacterListPresenter(this, Repository())

        initReciclerView()

        binding.scrollView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight)
            {
                page ++
                getChartersPage(page)
            }
        })
    }

    private fun initReciclerView(){
        adapter = CharacterAdapter(characterslist,adapterItemClickListener)
        binding.rvCharacter.layoutManager = LinearLayoutManager(requireContext())
        binding.rvCharacter.adapter = adapter
        getChartersPage(page)
    }

    private fun getChartersPage(page: Int) {
        binding.progressBar.visibility = View.VISIBLE

        presenter.getCharactersPage(page) { response ->
            response?.let {

                activity?.let {
                    requireActivity().runOnUiThread {
                        characterslist.addAll(response)
                        adapter.notifyDataSetChanged()
                        binding.progressBar.visibility = View.GONE
                    }
                }
            }
        }
    }

    companion object{
        private const val FIRTS_PAGE=1
        const val ARGUMENT_ID_CHARACTER = "arg_id_Character"
    }
}