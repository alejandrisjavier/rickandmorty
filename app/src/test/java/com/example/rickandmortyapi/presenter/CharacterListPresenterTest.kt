package com.example.rickandmortyapi.presenter

import android.util.Log
import android.view.View
import com.example.rickandmortyapi.CharacterListFragment
import com.example.rickandmortyapi.Contract.CharacterListContract
import com.example.rickandmortyapi.model.Character
import com.example.rickandmortyapi.model.Location
import com.example.rickandmortyapi.repository.Repository
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test


internal class CharacterListPresenterTest {

    @MockK
    private lateinit var presenter: CharacterListPresenter

    @Before
    @Throws(Exception::class)
    fun Setup() {
        MockKAnnotations.init(this)
        presenter = spyk(CharacterListPresenter(CharacterListFragment(), Repository()))

    }

    @Test
    fun getCharactersPage() {
        val characterslist = mutableListOf<Character>()

        coEvery {
            presenter.getCharactersPage(1) {
            }
        } returns characterslist

        presenter

    }
}

private infix fun <T, B> MockKStubScope<T, B>.returns(characterslist: MutableList<Character>): MutableList<Character> {
    val characterslist = mutableListOf<Character>()
    var character: Character = Character(
        1, "Evolved Narnian", "Alive", "Humanoid", "",
        "Male", Location("", ""),
        Location("", ""), "https://rickandmortyapi.com/api/character/avatar/683.jpeg",
        mutableListOf<String>(), "", ""
    )

    characterslist.add(character)
    return characterslist
}





